import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:weather/models/api/api_constant.dart';

class WeatherRepository {
  Duration  timeOut=Duration(seconds: 60);
  Future<Response> getCurrentWeatherByLatLonR(double lat,double lon) async {
    Response response = await Client().get(Uri.parse(
      "${ApiConstant.baseUrl}?appid=${ApiConstant.appid}&lat=$lat&lon=$lon")
    ).timeout(timeOut);
    return response;
  }
  Future<Response> getCurrentWeatherByCityNameR(String cityName) async {
    Response response = await Client().get(Uri.parse(
        "${ApiConstant.baseUrl}?appid=${ApiConstant.appid}&q=$cityName")
    ).timeout(timeOut);
    return response;
  }
}
