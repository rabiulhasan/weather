import 'package:geolocator/geolocator.dart';

class MyLocation {
  double? _lat;
  double? _lon;

  Future getLocation() async {
    try{
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      _lat=position.longitude;
      _lon=position.longitude;
      print(position);
    }
    catch(e)
    {
      print(e);
    }
   
  }

  double? get lat => _lat;

  double? get lon => _lon;
}