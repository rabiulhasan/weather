import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:weather/models/api/weather.dart';
import 'package:weather/repository/weather_repository.dart';

class WeatherProvider with ChangeNotifier {
  bool? _isLoading;
  MyWeather? _myWeather;

  void getCurrentLocation() {
    //MyLocation? location = MyLocation();
    // await location.getLocation();
    //location.lon!;location.lat!;
    double lon = 80;
    double lat = 30;
    getCurrentWeatherClt(lon, lat);
  }
 void fetchCurrentWeatherByCity(cityName)
 {
   double lon = 80;
   double lat = 30;
   getCurrentWeatherClt(lon, lat);
   getCurrentWeatherByCityClt(cityName);
 }
  Future<void>getCurrentWeatherClt(double lon,double lat) async {
    try {
      Response response = await WeatherRepository().getCurrentWeatherByLatLonR(lon,lat);
      if (response.statusCode == 200) {
         myWeather=MyWeather.fromJson(jsonDecode(response.body));
        isLoading=false;
      }
    } catch (ex) {
      print(ex.toString());

    }
  }
  Future<void>getCurrentWeatherByCityClt(String cityName) async {
    try {
      Response response = await WeatherRepository().getCurrentWeatherByCityNameR(cityName);
      //print(response);

      if (response.statusCode == 200) {
        myWeather=MyWeather.fromJson(jsonDecode(response.body));
        isLoading=false;
      }
    } catch (ex) {
      print(ex.toString());
    }
  }

  MyWeather get myWeather => _myWeather!;

  set myWeather(MyWeather value) {
    _myWeather = value;
    notifyListeners();
  }

  bool get isLoading => _isLoading!;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }
}
