
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather/pages/location_page.dart';
import 'package:weather/provider/weather_provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<WeatherProvider>(create: (context){
        return WeatherProvider();
      }),
      //Provider<WeatherProvider>(create: (context) => WeatherProvider()),

    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home:const LocationPage(), //LoadingPage(),
    );
  }
}
