import 'dart:convert';

import 'package:http/http.dart';
import 'package:weather/models/api/weather.dart';
import 'package:weather/repository/weather_repository.dart';

class WeatherController {
  final WeatherRepository weatherR = WeatherRepository();

  Future<MyWeather>getCurrentWeatherClt(double lon,double lat) async {
    try {
      Response response = await weatherR.getCurrentWeatherByLatLonR(lon,lat);
      //print(response);

      if (response.statusCode == 200) {
        return MyWeather.fromJson(jsonDecode(response.body));
      } else {
        return MyWeather();
      }
    } catch (ex) {
      print(ex.toString());
      return MyWeather();
    }
  }
  Future<MyWeather>getCurrentWeatherByCityClt(String cityName) async {
    try {
      Response response = await weatherR.getCurrentWeatherByCityNameR(cityName);
      //print(response);

      if (response.statusCode == 200) {
        return MyWeather.fromJson(jsonDecode(response.body));
      } else {
        return MyWeather();
      }
    } catch (ex) {
      print(ex.toString());
      return MyWeather();
    }
  }
}
