import 'package:flutter/material.dart';

import '../models/my_location.dart';

class TestLocationPage extends StatefulWidget {
  const TestLocationPage({Key? key}) : super(key: key);

  @override
  State<TestLocationPage> createState() => _TestLocationPageState();
}

class _TestLocationPageState extends State<TestLocationPage> {
  var locationMessage = "";

  Future getCurrentLocation() async {
    try {
      MyLocation? location = MyLocation();
      await location!.getLocation();
      location!.lat;
      location!.lon;
      print(location.lon);
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Geolocation',
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        leading: Icon(Icons.menu_sharp),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.location_on,
              color: Colors.blue,
              size: 50.0,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Get User Location',
                style: TextStyle(
                  fontSize: 30,
                )),
            SizedBox(
              height: 10.0,
            ),
            Text(locationMessage),
            SizedBox(
              height: 30.0,
            ),
            FlatButton(
              onPressed: () {
                getCurrentLocation();
              },
              color: Colors.blue,
              child: Text(
                "Get Current Location",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 100.0,
            ),
          ],
        ),
      ),
    );
  }
}
