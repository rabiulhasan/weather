import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather/pages/testing/page3.dart';

import 'package:weather/provider/weather_provider.dart';

import 'city_page.dart';

class LocationPage extends StatefulWidget {
  const LocationPage({Key? key}) : super(key: key);

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  @override
  void initState() {
    super.initState();
    var wprovider = Provider.of<WeatherProvider>(context, listen: false);
    wprovider.isLoading = true;
    wprovider.getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WeatherProvider>(
      builder: (_, weatherProvider, ___) {
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("assets/images/image-bg.jpg"),
              fit: BoxFit.cover,
            )),
            child: weatherProvider.isLoading == false
                ? SafeArea(
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TextButton(
                                onPressed: () {
                                  weatherProvider.isLoading = true;
                                  weatherProvider.getCurrentLocation();
                                },
                                child: Icon(
                                  Icons.near_me,
                                  size: 40,
                                )),
                            TextButton(
                                onPressed: () async {
                                  String? cityName =
                                      await Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                    return CityPage();
                                  }));
                                  if (cityName != null) {
                                    weatherProvider.isLoading = true;
                                    weatherProvider.fetchCurrentWeatherByCity(cityName);
                                  }
                                },
                                child: Icon(
                                  Icons.location_city,
                                  size: 40,
                                ))
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Image.network(
                            weatherProvider.myWeather.weather!.first.icon !=
                                    null
                                ? "http://openweathermap.org/img/wn/${weatherProvider.myWeather.weather!.first.icon!.toString()}.png"
                                : "",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            weatherProvider.myWeather.main!.temp != null
                                ? weatherProvider.myWeather.main!.temp!
                                    .toString()
                                : "",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            weatherProvider.myWeather.name != null
                                ? weatherProvider.myWeather.name!.toString()
                                : "",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            weatherProvider.myWeather.weather!.first.description?? "",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ),
        );
      },
    );
  }
}
