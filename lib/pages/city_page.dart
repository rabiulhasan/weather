import 'package:flutter/material.dart';
import 'package:weather/models/api/weather.dart';
import 'package:weather/pages/location_page.dart';

import '../controller/weather_controller.dart';
import '../models/my_location.dart';

class CityPage extends StatefulWidget {
  final MyWeather? weather;

  const CityPage({Key? key, this.weather}) : super(key: key);

  @override
  State<CityPage> createState() => _CityPageState();
}

class _CityPageState extends State<CityPage> {
  TextEditingController? cityEditingClt;


  @override
  void initState() {
    super.initState();
    cityEditingClt = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/city-bg.jpg"),
          fit: BoxFit.cover,
        )),
        child: SafeArea(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                  alignment: Alignment.topLeft,
                  child: TextButton(
                    onPressed: () {

                    },
                    child: Icon(
                      Icons.arrow_back,
                      size: 30,
                    ),
                  )),
              Container(
                padding: EdgeInsets.all(20),
                child: TextField(
                  controller: cityEditingClt,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    hintText: "Enter your city name",
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 24),
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  if (cityEditingClt!.text == "") {
                    print("Please Enter City Name");
                  } else {
                    Navigator.pop(context, cityEditingClt!.text);
                  }
                },
                child: Text(
                  "Get Weather",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
