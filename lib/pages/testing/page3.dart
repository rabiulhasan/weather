import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather/provider/weather_provider.dart';

class Page3 extends StatefulWidget {
  final String? data;
  const Page3({this.data,Key? key}) : super(key: key);

  @override
  State<Page3> createState() => _Page3State();
}

class _Page3State extends State<Page3> {
  String data="Test Data Pass";
  @override
  Widget build(BuildContext context) {
    return Consumer<WeatherProvider>(
      builder: (_,wp,___){
        return Scaffold(
          body: Center(
            child: TextButton(
              onPressed: (){

              },
              child: Text(wp.myWeather.weather!.first.description!),
            ),
          ),
        );
      },
    );
  }
}
